% --------------------------------
% LEI EXERCICIO 1

% Disable warnings and such

:- set_prolog_flag(discontiguous_warnings,off).
:- set_prolog_flag(single_var_warnings,off).
:- set_prolog_flag(unknown,fail).

:- op(900,xfy,'::').
:- dynamic filho/2.
:- dynamic pai/2.
:- dynamic avo/2.
:- dynamic irmao/2.
:- dynamic tio/2.
:- dynamic sobrinho/2.
:- dynamic primo/2.
:- dynamic bisavo/2.
:- dynamic bisneto/2.
:- dynamic naturalidade/2.
:- dynamic relacao/3.
:- dynamic descendente/2. 
:- dynamic ascendente/2.
:- dynamic listadescendentes/2.
:- dynamic listascendentes/2.

% Knowledge bases
filho(herb,abraham).
filho(herb,mona).
filho(homer,abraham).
filho(homer,mona).
filho(marge,clancy).
filho(marge,jackie).
filho(patty,clancy).
filho(patty,jackie).
filho(selma,clancy).
filho(selma,jackie).
filho(bart,homer).
filho(bart,marge).
filho(lisa,homer).
filho(lisa,marge).
filho(maggie,homer).
filho(maggie,marge).
filho(ling,selma).

% Regras de inferências
pai(P, F) :- filho(F, P).

avo(A, N) :- filho(N,X), pai(A,X).

neto(N, A) :- avo(A,N).

irmao(I1, I2) :- filho(I1,X), filho(I2,X), I1 \= I2.

tio(T,S) :- pai(P,S), irmao(P,T).

sobrinho(S, T) :- tio(T,S).

primo(P1, P2) :- pai(X,P1), tio(X,P2).


bisavo(B, BN) :- filho(X,B), avo(X,BN).

bisneto(BN, B) :- bisavo(B,BN).

%% Identificar relações entre individuos
relacao(A,B,R):-filho(A,B),'filho' = R.
relacao(A,B,R):-pai(A,B),'pai' = R.
relacao(A,B,R):-avo(A,B),'avo' = R.
relacao(A,B,R):-neto(A,B),'neto' = R.
relacao(A,B,R):-irmao(A,B),'irmao' = R.
relacao(A,B,R):-tio(A,B),'tio' = R.
relacao(A,B,R):-sobrinho(A,B),'sobrinho' = R.
relacao(A,B,R):-primo(A,B),'primo' = R.

relacao(A,B,R):-bisavo(A,B),'bisavo' = R.
relacao(A,B,R):-bisneto(A,B),'bisneto' = R.

%% Ascendentes e Descendentes
descendente(D,A,1) :- filho(D,A).
descendente(D,A,1) :- pai(A,D).
descendente(D,A,N) :- filho(D,P), descendente(P,A,R), N is R+1.
descendente(D,A,N) :- pai(A,P), descendente(D,P,R), N is R+1.
		
ascendente(X,Y,N):- descendente(Y,X,N).


listadescendentes(_,0,[]).				
listadescendentes(X,G,R) :-
		setof(Y,descendente(Y,X,G),R1), listadescendentes(X,G2,R2), concatenar(R2,R1,R),
			G is G2+1.

listascendentes(_,0,[]).	 
listascendentes(X,G,R) :-
		setof(Y,ascendente(Y,X,G),R1), listascendentes(X,G2,R2), concatenar(R2,R1,R),
			G is G2+1.

%% Regras auxiliares 
evolucao(Termo) :- solucoes(Invariante,+Termo::Invariante,Lista),
				   inserir(Termo),
				   teste(Lista).

regressao(T) :- remocao(T).

inserir(Termo) :- assert(Termo).
inserir(Termo) :- retract(Termo), !, fail.

remocao(T) :- retract(T).

teste([]).
teste([H|T]) :- H, teste(T).

comprimento([],0).
comprimento([H|T],R) :- comprimento(T,N), R is N+1.

solucoes(X,Y,Z) :- findall(X,Y,Z).

concatenar([],L2,L2).
concatenar([X|R],L2,[X|L]) :- concatenar(R,L2,L).

%% Invariantes
% Impedir conhecimento repetido
+filho(F,P) :: (solucoes((F,P), (filho(F,P)), S),
			   comprimento(S,N),
			   N==1).

+pai(P,F) :: (solucoes((P,F), (pai(P,F)), S),
			   comprimento(S,N),
			   N==1).

+naturalidade(P,L) :: (solucoes((P,L), (naturalidade(P,L)), S),
				comprimento(S,N),
				N==1).				  

% Impedir filhos com mais do que 2 pais
+filho(F,P) :: (solucoes((Ps), (filho(F,Ps)), S),
				comprimento(S,N),
				N=<2).

+naturalidade(P,L) :: (solucoes((Ls), (naturalidade(P,Ls)), S),
				comprimento(S,N),
				N==1). 
